# About How to use CMake in C++  
2022-11-08  12:36  by  CYL  @ST
## Official Website
1.https://cmake.org/cmake/help/latest/guide/tutorial/index.html  
2.https://cmake.org/cmake/help/latest/

## About
This doc is about cmake internal build.

## The C++ File Content  
``` C++
#include <iostream>
using namespace std;
int main(){
	string str="Hello,C++!";
	cout<<str<<endl;
	return 0;
}
```

## The CMakeLists.txt File Content  
``` bash
#CMakeList.txt
PROJECT (HELLO_C++)
SET (SRC_LIST Hello.cpp)
MESSAGE (STATUS "This is BINARY dir" ${HELLO_C++_BINARY_DIR})
MESSAGE (STATUS "This is SOURCE dir" ${HELLO_C++_SOURCE_DIR})
ADD_EXECUTABLE (HELLO_C++ ${SRC_LIST})
```
Then we should input CLI Like:
``` bash
cmake /<CMakeList.txt File Path>/
```
After this,we need type CLI like:
``` bash
make /<MakeFile File Path>/
```
At last,run the executable application
``` bash
./HELLO_C++
```

## The Detail Description of CMakeLists.txt
1. ``` bash
	PROJECT (HELLO_C++)  
	```
	This statement means your Project Name is HELLO_C++.  
	You can define which programming language have used in this project,such as:  
	- PROJECT (HELLO_C++ CXX)  
	means you used C++ in this project.  
	- PROJECT (HELLO_C++ C CXX)  
	means you used C and C++ in this project.  
	- PROJECT (HELLO_C++ JAVA)  
	means you used JAVA in this project.  
2. ``` bash
   MESSAGE (STATUS "This is BINARY dir" ${HELLO_C++_BINARY_DIR})  
   MESSAGE (STATUS "This is SOURCE dir" ${HELLO_C++_SOURCE_DIR})  
   ```
	- <projectname_BINARY_DIR>\<projectname_SOURCE_DIR> can be used by MESSAGE.  
3. ``` bash
	SET (SRC_LIST Hello.cpp)  
	```
	- means set SRC_LIST as Hello.cpp.  
	- we can also do like this: SET (SRC_LIST Hello.cpp demo1.cpp demo2.cpp)  
4. ``` bash
	MESSAGE (STATUS "This is BINARY dir" ${HELLO_C++_BINARY_DIR})  
	```
	- MESSAGE has three status:  
	a. SEND_ERROR:skip output.  
	b. STATUS: Output message what you write.  
	c. FATAL_ERROR:stop cmake process.  
5. ``` bash
	ADD_EXECUTABLE (HELLO_C++ ${SRC_LIST})  
	```
	- means output an executable file which named HELLO_C++,the source file is content of SRC_LIST.  

![_0_cmake.png][pic0]

[pic0]: ../cmakePic/_0_cmake.png