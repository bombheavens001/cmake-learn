## CLI statement
``` bash
    mkdir _3_Temp_BuildAndInstall
    cd _3_Temp_BuildAndInstall
    mkdir src
    mkdir build
    mkdir DocCYL
    gedit CMakeLists.txt
```

## Content of CMakeLists.txt
``` bash
# CMakeList.txt
# 2022-11-09  by  CYL  @ST
PROJECT (HELLO_C++)
# run CMakeLists.txt which under '/src/' and output at '/build/bin/'
ADD_SUBDIRECTORY (src bin)

# if do not set, CMAKE_INSTALL_PREFIX default value is '/usr/local/'
SET(CMAKE_INSTALL_PREFIX INS_CYL)
# copy FILES to '/build/INS_CYL/HW_CYL/'
INSTALL (FILES COPYRIGHT_CYL readme.md DESTINATION HW_CYL)
# copy PROGRAMS like bash or python file to '/build/INS_CYL/bin/'
INSTALL (PROGRAMS runHello.sh DESTINATION bin)
# copy all files under DocCYL to 'build/INS_CYL/HW_CYL/'.
# if you write INSTALL (DIRECTORY DocCYL DESTINATION HW_CYL),it means cmake will move DocCYL File to 'build/INS_CYL/HW_CYL/'.
INSTALL (DIRECTORY DocCYL/ DESTINATION HW_CYL)
```

## CLI statement
``` bash
    touch runHello.sh
    touch COPYRIGHT_CYL
    cd src 
    gedit Hello.cpp
    gedit CMakeLists.txt
```
## Content of CMakeLists.txt
``` bash
    ADD_EXECUTABLE (APP Hello.cpp)
```

## CLI statement
```
    cd ..
    cd DocCYL
    touch DOCCYL_AA.txt
    cd ..
    cd build
```

## How to install By using CMake
- After doing cmake,we will get MakeFile,using make to build project.
- then we can use 
    ``` bash
    sudo make install
    ```
    to install the application.
    someting like https://docs.opencv.org/4.x/d7/d9f/tutorial_linux_install.html
- Also,we can use
    ``` bash
    sudo make install DESTDIR=/the Path you wanna install/
    ```
    to install the application at the path where you wanna.

## the default install path
/usr/local
see also https://docs.opencv.org/4.x/db/d05/tutorial_config_reference.html

## CLI statement
``` bash
 cmake ..
 make 
 make install 
```
## The File Tree
``` bash
.
├── build
│   ├── bin
│   │   ├── APP
│   │   ├── CMakeFiles
│   │   │   ├── APP.dir
│   │   │   │   ├── build.make
│   │   │   │   ├── cmake_clean.cmake
│   │   │   │   ├── CXX.includecache
│   │   │   │   ├── DependInfo.cmake
│   │   │   │   ├── depend.internal
│   │   │   │   ├── depend.make
│   │   │   │   ├── flags.make
│   │   │   │   ├── Hello.o
│   │   │   │   ├── link.txt
│   │   │   │   └── progress.make
│   │   │   ├── CMakeDirectoryInformation.cmake
│   │   │   └── progress.marks
│   │   ├── cmake_install.cmake
│   │   └── Makefile
│   ├── CMakeCache.txt
│   ├── CMakeFiles
│   │   ├── 3.10.2
│   │   │   ├── CMakeCCompiler.cmake
│   │   │   ├── CMakeCXXCompiler.cmake
│   │   │   ├── CMakeDetermineCompilerABI_C.bin
│   │   │   ├── CMakeDetermineCompilerABI_CXX.bin
│   │   │   ├── CMakeSystem.cmake
│   │   │   ├── CompilerIdC
│   │   │   │   ├── a.out
│   │   │   │   ├── CMakeCCompilerId.c
│   │   │   │   └── tmp
│   │   │   └── CompilerIdCXX
│   │   │       ├── a.out
│   │   │       ├── CMakeCXXCompilerId.cpp
│   │   │       └── tmp
│   │   ├── cmake.check_cache
│   │   ├── CMakeDirectoryInformation.cmake
│   │   ├── CMakeOutput.log
│   │   ├── CMakeTmp
│   │   ├── feature_tests.bin
│   │   ├── feature_tests.c
│   │   ├── feature_tests.cxx
│   │   ├── Makefile2
│   │   ├── Makefile.cmake
│   │   ├── progress.marks
│   │   └── TargetDirectories.txt
│   ├── cmake_install.cmake
│   ├── INS_CYL
│   │   ├── bin
│   │   │   └── runHello.sh
│   │   └── HW_CYL
│   │       ├── COPYRIGHT_CYL
│   │       ├── DOCCYL_AA.txt
│   │       └── readme.md
│   ├── install_manifest.txt
│   └── Makefile
├── CMakeLists.txt
├── COPYRIGHT_CYL
├── DocCYL
│   └── DOCCYL_AA.txt
├── readme.md
├── runHello.sh
└── src
    ├── CMakeLists.txt
    └── Hello.cpp

16 directories, 49 files
```