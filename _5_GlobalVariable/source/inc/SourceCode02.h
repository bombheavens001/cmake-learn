#ifndef _SOURCECODE02_H
#define _SOURCECODE02_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct glbData02Struct {
    unsigned int ID;
    char name[20];
} gData02_S;

void initData02_S(gData02_S *data);
void deleteData02_S(gData02_S *data);
void set_g_Data02(gData02_S *data);
gData02_S *get_g_Data02(void);

#endif
