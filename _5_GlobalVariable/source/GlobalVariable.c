#include "GlobalVariable.h"
/**
*	2022-11-17	16:40	by	CYL
*	@TIP:
*		This File is the main C File of 'GlobalVariable' Application.
*	@AIM: 
*		To use the Global Variable which from other .c File without using 'extern Global_Variable;'
*	@Related Files:
*		1.SourceCode01.c
*		2.SourceCode02.c
*		3.related CMakeLists.txt
*/
int main(int argc, char**argv)
{
    /**
    *   To test SourceCode01.c
	*	[Attention]:Global Variable must created as a const variable(not the const c type).
    */
    setNumb01(5);
    printf("getNumb01()'s result is %d\n",getNumb01());

    /**
	*	2022-11-17	16:27	by	CYL
    *   To test SourceCode02.c
	*	[A1-1]
	*	If you wanna Set the Global Structure Point Variable(*g_Data02) which from other .c File(SourceCode02.c),
	*	you should use malloc() or calloc() to create a Structure Point Variable to changed it,
	*	such as 'gData02_S *gMainData = (gData02_S *)calloc(1,sizeof(gData02_S));'.
	*	[A2]
	*	And do not forget using free() function.
    */
    gData02_S *gMainData = (gData02_S *)calloc(1,sizeof(gData02_S));	//[A1-1]Initialized 'gMainData'
    gMainData->ID = 99;
    strcpy(gMainData->name,"laotie!");
    set_g_Data02(gMainData);
	
	/*
	*	[A1-2]
	*	If you just wanna get the Global Structure Point Variable(*g_Data02) which from other .c File(SourceCode02.c),
	*	you can just create a Structure Point Variable directly,such as 'gData02_S *gShowData = NULL;'.		
	*/
    gData02_S *gShowData = NULL;										//[A1-2]Initialized 'gShowData'
    gShowData = get_g_Data02();
    printf("get_g_Data02()->ID's result is %d\n",gShowData->ID);
    printf("get_g_Data02()->name's result is %s\n",gShowData->name);
    gShowData=NULL;
    /**
    *   To Show gMainData values before and after deleted.
    */
    printf("gMainData->ID = %d\n",gMainData->ID);
    printf("gMainData->name is %s\n",gMainData->name);
    deleteData02_S(gMainData);	//[A2]
    printf("gMainData->ID after deleted = %d\n",gMainData->ID);
    printf("gMainData->name after deleted is %s\n",gMainData->name);

	/*
	*	Task: use the GDB to see the content value of 'gShowData/gMainData',
	*		and their relation which about Point Address between the 'g_Data02' from SourceCode02.c.
	*		before and after using set_g_Data02() ;
	*		before and after using get_g_Data02();
	*		before and after using deleteData02_S();
	*	GDB CLI: help \list \ break x(Line Number) \ run \ print (null,* or & just as C)variable \ continue \ step .
	*/
    return 0;
}
