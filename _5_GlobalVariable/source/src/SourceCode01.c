#include "SourceCode01.h"
/**
*	2022-11-17	15:54	by	CYL
*	The global Variable(g_Numb01) just can be used in this .c File(SourceCode01.c).
*	If other .c File want to change g_Numb01's value,
*	you should use API(see void setNumb01(unsigned int value)) to change g_Numb01's value.
*
*	[Example]:See GlobalVariable.c File.
*/
unsigned int g_Numb01 = 0;	//The Global Variable.
/**
*	@Version: v1.0 2022-11-17 16:02 by CYL
*	@Name: setNumb01.
*	@Para: value; Type: as same as 'g_Numb01'.
*	@Return: NULL.
*	@Means: If you want to change the value of 'g_Numb01'(The Global Variable), you should use this function.
*/
void setNumb01(unsigned int value)
{
    g_Numb01 = value;
}
/**
*	@Version: v1.0 2022-11-17 16:02 by CYL
*	@Name: getNumb01.
*	@Para: NULL.
*	@Return: g_Numb01(The Global Variable).
*	@Means: If you want to get the value of 'g_Numb01'(The Global Variable), you should use this function.
*/
unsigned int getNumb01(void)
{
    return g_Numb01;
}
