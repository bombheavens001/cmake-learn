#include "SourceCode02.h"
/**
*	2022-11-17	16:03	by	CYL
*	When the Global Variable is a Structure.
*	If you wannt to set a Global Variable as a Point Variable of Structure(*g_Data02),
*	you should create a Const Global Variable of Structure(g_Data02Gen) at First.
*	Then give 'the Address about a Const Global Variable of Structure which you created'(&g_Data02Gen) to 'the Point Variable of Structure'(*g_Data02).
*	
*	[Attention]:Global Variable must be a const variable.
*				And try to avoid using 'extern Global Variable;' in other .h Files.
*				You should share the Global Variable as a function(API),such as set_g_Data02() and get_g_Data02() in this .c File.
*	[Example]:See GlobalVariable.c File.
*/
gData02_S g_Data02Gen = {			//a Const Global Variable of Structure,just can be used by 'g_Data02'.
	0,
	"Zero"
};
gData02_S *g_Data02 = &g_Data02Gen;	//a Global Point Variable of Structure,can be used by others.

/**
*	2022-11-17	16:11	by	CYL
*	Initialize the Structure which Typed as 'gData02_S'.
*/
void initData02_S(gData02_S *data)
{
    memset(data,0,sizeof(data));
}
/**
*	2022-11-17  16:13  by  CYL
*	Free the Point Variable which created by malloc() or calloc().
*/
void deleteData02_S(gData02_S *data)
{
    free(data);
    data=NULL;
}
/**
*	2022-11-17	16:14	by	CYL
*	Change the value of Global Structure Point in this File(which named 'g_Data02').
*/
void set_g_Data02(gData02_S *data)
{
    initData02_S(g_Data02);
    g_Data02->ID = data->ID;
    strcpy(g_Data02->name,data->name);
}
/**
*	2022-11-17	16:18	by	CYL
*	Get the value of Global Structure Point in this File(which named 'g_Data02').
*/
gData02_S *get_g_Data02(void)
{
    return g_Data02;
}
