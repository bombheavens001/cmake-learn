2022-11-09  19:07  by  CYL  @ST
## CMake build Project with Libs
- static Libs File Suffix: '.a' or '.lib'.
- dynamic Libs File Suffix: '.so' or '.dll'.
- static Libs will include into application when cmake build the project.If you run application,
    application can operate by itself.
- dynamic Libs do not include into application when cmake build the project.If you run application,
    application should operate with dynamic Libs.

## Create a dynamic Libs and static Libs
The src File Tree:
``` bash
    |--src
        |--inc
            |--helloCpp.h
        |--lib
            |--helloCpp.cpp
            |--CMakeLists.txt
        |--main.cpp
        |--CMakeLists.txt
```
- we need to create a dynamic Libs at first.
    helloCpp.cpp
    ``` C++ 
        #include "helloCpp.h"
        #include <iostream>
        using namespace std;
        void HelloWorld(){
            cout<<"Hello World!This is C++11!"<<endl;
        }
    ```
- Then we need to create a dynamic Libs' Header File.
    helloCpp.h
    ``` C++
        #ifndef _HELLOCPP_H
        #define _HELLOCPP_H

        #ifdef __cplusplus
        extern "C"{
        #endif


        void HelloWorld(void);


        #ifdef __cplusplus
        }
        #endif

        #endif
    ```
- Then we should create a CMakeLists.txt which under lib path to build .so and .a File.
``` Bash
    # 2022-11-09  16:16  by  CYL  @ST
    # add the .h file used in helloCpp.cpp.
    INCLUDE_DIRECTORIES (../inc/)
    # name .so Source CXX file as 'libhellocpp_src'.
    SET(libhellocpp_src helloCpp.cpp)

    # name .so File as 'hellocpp',it will be renamed as 'lib'+'hellocpp'+'.so'.
    # 'SHARED' will output a .so file.(dynamic libs).
    ADD_LIBRARY(hellocpp SHARED ${libhellocpp_src})
    # rename 'libhellocpp.so' as 'libhellocpp.so'.
    SET_TARGET_PROPERTIES(hellocpp PROPERTIES OUTPUT_NAME "hellocpp")
    SET_TARGET_PROPERTIES(hellocpp PROPERTIES CLEAN_DIRECT_OUTPUT 1)

    # 'STATIC' will output a .a file.(static libs).
    ADD_LIBRARY(hellocpp_static STATIC ${libhellocpp_src})
    # rename 'libhellocpp_static.a' as 'libhellocpp.a'.
    SET_TARGET_PROPERTIES(hellocpp_static PROPERTIES OUTPUT_NAME "hellocpp")
    SET_TARGET_PROPERTIES(hellocpp_static PROPERTIES CLEAN_DIRECT_OUTPUT 1)
```

## Create a main function file
- The main.cpp content:
``` C++
    #include "helloCpp.h"
    int main(){
        HelloWorld();
        return 0;
    }
```
- Create a CMakeLists.txt which under src path to build executable file.
1. The version to build executable with .a file. -->The File Tree see at FileTreeStatic.txt .
    ``` bash
        # run CMakeLists.txt which under '_4_Temp_BuildWithLibs/src/lib/' and output .so or .a file at '/build/bin/libs/'
        ADD_SUBDIRECTORY (lib libs)

        # add the .h files which used in APP_CYL.
        INCLUDE_DIRECTORIES (inc)
        # output an executable application which named APP_CYL.
        ADD_EXECUTABLE (APP_CYL main.cpp)

        # you can also add libs(.a or .so ..) by using LINK_DIRECTORIES (/the path of libs which existed./)
        # LINK libhellocpp.so File which created by CMakeLists.txt under '_4_Temp_BuildWithLibs/src/lib/' path.
        # hellocpp was created by CMakeLists.txt under '_4_Temp_BuildWithLibs/src/lib/' path.
        # TARGET_LINK_LIBRARIES (APP_CYL hellocpp)

        # Also,you can use .a file,such as:
        # hellocpp_static was created by CMakeLists.txt under '_4_Temp_BuildWithLibs/src/lib/' path.
        TARGET_LINK_LIBRARIES (APP_CYL hellocpp_static)
    ```
2. The version to build executable with .so file. -->The File Tree see at FileTreeDynamic.txt .
    ``` bash
        # run CMakeLists.txt which under '_4_Temp_BuildWithLibs/src/lib/' and output .so or .a file at '/build/bin/libs/'
        ADD_SUBDIRECTORY (lib libs)

        # add the .h files which used in APP_CYL.
        INCLUDE_DIRECTORIES (inc)
        # output an executable application which named APP_CYL.
        ADD_EXECUTABLE (APP_CYL main.cpp)

        # you can also add libs(.a or .so ..) by using LINK_DIRECTORIES (/the path of libs which existed./)
        # LINK libhellocpp.so File which created by CMakeLists.txt under '_4_Temp_BuildWithLibs/src/lib/' path.
        # hellocpp was created by CMakeLists.txt under '_4_Temp_BuildWithLibs/src/lib/' path.
        TARGET_LINK_LIBRARIES (APP_CYL hellocpp)

        # Also,you can use .a file,such as:
        # hellocpp_static was created by CMakeLists.txt under '_4_Temp_BuildWithLibs/src/lib/' path.
        # TARGET_LINK_LIBRARIES (APP_CYL hellocpp_static)
    ```

## Create a CMakeLists.txt under _4_Temp_BuildWithLibs to set the cmake.
``` bash
    |--_4_Temp_BuildWithLibs
        |--src
        |--build
        |--CMakeLists.txt
        |--readme.md
```
- Content of CMakeLists.txt under _4_Temp_BuildWithLibs path.
    ``` bash
        # CMakeList.txt

        # 2022-11-09  10:40  by  CYL  @ST
        CMAKE_MINIMUM_REQUIRED(VERSION 3.10)
        PROJECT (HELLO_C++)
        SET(CMAKE_INSTALL_PREFIX INSCYL)

        # create a file which named /bin in /build path ,the content is from srcs' CMakeLists.txt
        ADD_SUBDIRECTORY (src bin)
    ```

## CLI statement
``` bash
 cd build 
 cmake ..  #means cmake the CMakeLists.txt under _4_Temp_BuildWithLibs/ path.
 make 
 cd bin
 ./APP_CYL
```

## The procedure of CMake(Attention!)
1. _4_Temp_BuildWithLibs/CMakeLists.txt find 'ADD_SUBDIRECTORY (src bin)',then it will 'cd /src' to find CMakeLists.txt under /src,and 'mkdir bin' under build path.
2. /src/CMakeLists.txt find 'ADD_SUBDIRECTORY (lib libs)',then it will 'cd lib' to find CMakeLists.txt under /lib,and 'mkdir libs' under /build/bin path.
3. /lib/CMakeLists.txt will build .so or .a file,and output files under /build/bin/libs.

## Differ from 'make install'
'INSTALL' means install .h .a .so files to system path,such as "hello.h" can be used as <hello.h> in C/C++ Files.
We need 'CMAKE_INSTALL_PREFIX' to tell cmake where we wanna install.
'CMAKE_INSTALL_PREFIX' default value is /usr/local.we can change the value in CMakeLists.txt under '_4_Temp_BuildWithLibs' path like this:
``` bash
    SET(CMAKE_INSTALL_PREFIX INSCYL)
```