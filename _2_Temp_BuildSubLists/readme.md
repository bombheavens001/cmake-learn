2022-11-08  19:59  by  CYL  @ST
## The File Tree
```
├── build
│   ├── bin
│   │   ├── APP
│   │   ├── CMakeFiles
│   │   │   ├── APP.dir
│   │   │   │   ├── build.make
│   │   │   │   ├── cmake_clean.cmake
│   │   │   │   ├── CXX.includecache
│   │   │   │   ├── DependInfo.cmake
│   │   │   │   ├── depend.internal
│   │   │   │   ├── depend.make
│   │   │   │   ├── flags.make
│   │   │   │   ├── Hello.o
│   │   │   │   ├── link.txt
│   │   │   │   └── progress.make
│   │   │   ├── CMakeDirectoryInformation.cmake
│   │   │   └── progress.marks
│   │   ├── cmake_install.cmake
│   │   └── Makefile
│   ├── CMakeCache.txt
│   ├── CMakeFiles
│   │   ├── 3.10.2
│   │   │   ├── CMakeCCompiler.cmake
│   │   │   ├── CMakeCXXCompiler.cmake
│   │   │   ├── CMakeDetermineCompilerABI_C.bin
│   │   │   ├── CMakeDetermineCompilerABI_CXX.bin
│   │   │   ├── CMakeSystem.cmake
│   │   │   ├── CompilerIdC
│   │   │   │   ├── a.out
│   │   │   │   ├── CMakeCCompilerId.c
│   │   │   │   └── tmp
│   │   │   └── CompilerIdCXX
│   │   │       ├── a.out
│   │   │       ├── CMakeCXXCompilerId.cpp
│   │   │       └── tmp
│   │   ├── cmake.check_cache
│   │   ├── CMakeDirectoryInformation.cmake
│   │   ├── CMakeOutput.log
│   │   ├── CMakeTmp
│   │   ├── feature_tests.bin
│   │   ├── feature_tests.c
│   │   ├── feature_tests.cxx
│   │   ├── Makefile2
│   │   ├── Makefile.cmake
│   │   ├── progress.marks
│   │   └── TargetDirectories.txt
│   ├── cmake_install.cmake
│   └── Makefile
├── CMakeLists.txt
├── readme.md
└── src
    ├── CMakeLists.txt
    └── Hello.cpp

12 directories, 41 files
```
## About
- we need 2 CMakeLists.txt to build the project.
- we put .cpp files in src path,and in src path we also need a CMakeLists.txt.
- we need to use cmake in build path.(so we need CMakeLists.txt in root path--_2_Temp_Build)

## CLI statement
start:
``` bash
    mkdir _2_Temp_Build
    cd _2_Temp_Build
    mkdir src
    mkdir build
    gedit CMakeLists.txt
```
## Content of CMakeLists.txt in root path
``` bash
    #CMakeList.txt
    PROJECT (HELLO_C++)
    ADD_SUBDIRECTORY (./src ./bin)
```
- Add a subdirectory to the build.
    add_subdirectory(source_dir [binary_dir] [EXCLUDE_FROM_ALL] [SYSTEM])  
- See the official website commentate:https://cmake.org/cmake/help/latest/command/add_subdirectory.html?highlight=add_subdirectory

## CLI statement
``` bash
    cd src
    gedit Hello.cpp
    gedit CMakeLists.txt
```
## Content of CMakeLists.txt in src path
``` bash
    ADD_EXECUTABLE (APP Hello.cpp)
```
## CLI statement
build project and run application:
``` bash
    cd ..
    cd build
    cmake ..
    make
    cd bin
    ./APP
```